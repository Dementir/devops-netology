# devops-netology

## Ignored files
.terraformrc
terraform.rc
override.tf
override.tf.json
crash.log

все файлы, которые оканчиваются на:
* _override.tf
* _override.tf.json
* .tfvars
* .tfstate

Все файлы, которые имеют в названии следующее - `.tfstate.`

И Все что лежит в папке `**/.terraform/*`

